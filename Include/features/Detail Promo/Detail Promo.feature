@tag
Feature: Detail Promo

	Scenario Outline: User check detail promo
    Given I start application
    Given ganti wifi
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to check detail promo
    Then I saw detail promo

    Examples: 
      | username   | password   | pin 	 |
      | bribri0001 | Jakarta123 | 123457 |